# Atividade Prática
Respostas das questões práticas no README, seguindo a questão 2.

## Questão 3
Esse código é executado em nosso terminal a partir do comando **nodejs calculadora.js n1 n2**.

## Questão 4
Para commitar apenas um arquivo, ao invés de digitar **git add .**, basta digitar **git add [nome do arquivo]**, e, posteriormente, fazer um git commit normalmente.
No caso do arquivo calculadora.js, o comando seria **git add calculadora.js**.

Esse commit, segundo a convenção, deve ser **git commit -m "feat: calculadora nova"**.

Já o commit do README.md, deve ser **git commit -m "feat: respostas questões 3 e 4"**.

## Questão 5
Relacionado ao conventional commit, esse commit continua sendo feat, alterando apenas a descrição posterior, tendo em vista que adicionou um novo recurso ao código, uma nova função, e não corrigiu nenhum problema, ficando, portanto, dessa maneira: **git commit -m "feat: nova função soma"**.

## Questão 6
Foram corrigidos a posição de "args" nas funções soma e sub, e foi corrigido também o console do default (linha 21).
O commit ficou da seguinte maneira: **git commit -m "fix: soma, sub e default(linha 21) corrigidos"**.

## Questão 7
Para criar uma nova branch e trabalhar na mesma, usei o comando **git checkout -b develop**

## Questão 8
Para fazer o merge, foi feita uma merge request no gitlab, e, depois que ela foi aprovada, aconteceu o merge.

## Questão 9
Para o revert as alterações, é possível fazer através do próprio gitlab, indo no commit que deseja reverter, e clicando na opção "revert", ou então é possível reverter também através do comando **git revert *SHA do commit***.

## Questão 10
Após acrescentar **const args = process.argv.slice(2);** na linha 1, para instanciar a variável args e desconsiderar os 2 primeiros argumentos, para executar o código basta digitar **nodejs calculadora.js n1 "operador" n2**, e, dessa forma, ele retornará a operação desejada. Entretanto, ele não realiza a operação de multiplicação.

A função evaluate, executa a função eval do javascript, que avalia um expressão retornando uma cadeia de caracteres ou um número.